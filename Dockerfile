FROM node:lts-alpine AS builder
WORKDIR /usr/src/app
COPY . .
RUN npm install
RUN npm run clean
RUN npm run test
RUN npm run build

FROM node:lts-alpine AS release
WORKDIR /usr/src/app
COPY --from=builder ./usr/src/app/build ./build
COPY package*.json ./
RUN npm install --production
CMD [ "node", "build/index.js" ]
