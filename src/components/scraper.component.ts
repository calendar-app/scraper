import moment from 'moment';
import cron from 'node-cron';
import { CalendarApiService, SheetsApiService } from '../services';
moment.locale('it');

export class Scraper {
  init() {
    console.log('\nScraper started!');
    console.log('Executing every 10th minute past every hour from 6 through 23');

    // https://crontab.guru/#*/10 6-23 * * *
    cron.schedule('*/10 6-23 * * *', () => this.scrape());
  }

  async scrape() {
    console.log(`\nStarting scraping at ${moment().format('DD/MM/YYYY HH:mm:ss')}`);

    try {
      const sheetsApiService = new SheetsApiService();
      const sheetList: string[] = await sheetsApiService.getSheetsList();

      let sheetDataPromises: Promise<any>[] = [];
      sheetList.forEach(sheetId => sheetDataPromises.push(sheetsApiService.getSheetData(sheetId)))
      const sheetData = await Promise.all(sheetDataPromises);
      const resultData = this.flatten(sheetData.map(item => item.rows = this.prepareData(item.sheetId, item.rows)));

      if (resultData.length != 0) {
        const calendarApiService = new CalendarApiService();
        await calendarApiService.postNewData(resultData);
      }

    } catch (err) {
      throw new Error(err)
    }
  }

  prepareData(sheetId: string, rows: string[]) {
    var result = [];
    for (var i = 1; i < rows.length; i++) {
      var item = rows[i];

      var courseName = sheetId;	// CloD
      var timeStart = item[4];	// 9.00
      var timeEnd = item[5];		// 13.00
      var date = moment(item[6], 'DD/MM/YYYY').utc().startOf('day').toISOString();   // 04/12/2019
      var day = item[7];			// lun
      var professor = item[8];	// Cognome Nome
      var module = item[9];		// Networking
      var room = item[10];		// FAD/L10
      var required = false;

      result.push({ courseName, timeStart, timeEnd, date, required, module, professor, room });
    }
    return result;
  }

  flatten(arr: any) {
    return arr.reduce((flat: any, toFlatten: any) =>
      flat.concat(Array.isArray(toFlatten) ? this.flatten(toFlatten) : toFlatten), []);
  }
}

