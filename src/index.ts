import { config } from 'dotenv';
import { Scraper } from './components';
import { CalendarApiService, SheetsApiService } from './services';

// import envs
config();

const sheetApi = new SheetsApiService();
const calendarApi = new CalendarApiService();
const scraper = new Scraper();

sheetApi.initTest()
  .then(_ => calendarApi.initTest())
  .then(_ => scraper.init())
  .catch(err => {
    console.error(err);
    process.exit(1);
  })
