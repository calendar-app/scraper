import axios, { AxiosResponse } from 'axios';

const base_url = process.env.API_ADDRESS; // http(s)://calendar.domain.xyz:1234

export class CalendarApiService {
  async initTest() {
    return axios({
      url: `${base_url}/health`,
      method: 'GET',
      headers: { 'Content-Type': 'application/json' }
    })
      .then((res: AxiosResponse<{ ok: boolean }>) => {
        if (res.data && res.data.ok) {
          console.log('CalendarApiService\t *OK*');
        }
      })
      .catch(err => { throw new Error('CalendarApiService: ' + err) })
  }

  async postNewData(data: any) {
    return axios({
      url: `${base_url}/`,
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      data
    })
      .then((res: AxiosResponse<any>) => {
        console.log(`Insert: ${res.data.length} items`)
        return res.data;
      })
      .catch(err => { throw new Error('CalendarApiService: ' + err) })
  }
}
