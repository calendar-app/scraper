import { JWT } from 'google-auth-library';
import { google, sheets_v4 } from 'googleapis';

export class SheetsApiService {
  static jwtClient: any;
  static sheets: sheets_v4.Sheets;

  async initTest() {
    await this.ensureGoogleApi();
    try {
      await SheetsApiService.sheets.spreadsheets.get({ spreadsheetId: process.env.SPREADSHEET_ID });
      console.log('SheetsApiService\t *OK*');
    } catch (err) {
      throw new Error('SheetsApiService: ' + err)
    }
  }

  async ensureGoogleApi() {
    const SERVICE_ACCOUNT = JSON.parse(process.env.GOOGLE_SERVICE_ACCOUNT);

    if (!SERVICE_ACCOUNT) {
      throw 'Service Account env not found!'
    }
    if (!SheetsApiService.jwtClient) {
      SheetsApiService.jwtClient = await this.getJwtClient(SERVICE_ACCOUNT);
    }
    if (!SheetsApiService.sheets) {
      SheetsApiService.sheets = google.sheets({ version: 'v4', auth: SheetsApiService.jwtClient });
    }
  }

  async getJwtClient(SERVICE_ACCOUNT: any): Promise<JWT> {
    return new google.auth.JWT(
      SERVICE_ACCOUNT.client_email, '',
      SERVICE_ACCOUNT.private_key,
      ['https://www.googleapis.com/auth/spreadsheets'],
      SERVICE_ACCOUNT.user,
    );
  }

  async getSheetsList(): Promise<string[]> {
    try {
      const result = await SheetsApiService.sheets.spreadsheets.get({
        spreadsheetId: process.env.SPREADSHEET_ID
      });
      return result.data.sheets.map(sheet => sheet.properties.title)
    } catch (err) {
      throw new Error('SheetsApiService: ' + err)
    }
  }

  async getSheetData(sheetId: string): Promise<any> {
    try {
      const result = await SheetsApiService.sheets.spreadsheets.values.get({
        spreadsheetId: process.env.SPREADSHEET_ID,
        range: sheetId
      });
      return {
        sheetId,
        rows: result.data.values
      };
    } catch (err) {
      throw new Error('SheetsApiService: ' + err)
    }
  }
}
