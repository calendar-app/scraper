# ITS Kennedy timetable scraper
This is the server used for parsing timetables given by the ITS Kennedy.

## How does it work
The service scrape the timesheet every 10 minutes and send the data to `API_ADDRESS`.
When the service starts, it opens the `SPREADSHEET_ID` file and get the name of the course, teacher name, class, time and room number.

## Requirements
1. **service-account.json**: Here are few steps for generating the file:
	1. Go to console.developers.google.com/apis/credentials
	2. On the top left there is a blue "create credentials" button click it and select "service account key." (see below if its not there)
	3. Choose the service account you want, and select "JSON" as the key type.
	4. It should allow give you a json to download

2. **`SPREADSHEET_ID`**: The unique id of the spreadsheet (https://docs.google.com/spreadsheet/`YOUR_SPREADSHEET_ID`/)
3. **`API_ADDRESS`**: Address of the API (micro)service

## Running the container
Use the `docker-compose.yml` you find in the (main repo)[https://gitlab.com/calendar-app/monorepo].
> Make sure to mount the `service-account.json` file to `/usr/src/app/service-account.json` in order to use google spreadsheet's APIs.

## Additional info
- Google service account: https://github.com/juampynr/google-spreadsheet-reader
- Original repo: https://gitlab.com/calendar-app/scraper_old
